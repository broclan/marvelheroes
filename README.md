# Marvel Heroes

In this app, the MVC architecture is used. Having the View Controllers being the intermediaries of the Model (service data) and the View (custom views) relationship. The chosen language was Swift and the target iOS was 11.2. Therefore, using Swift 4 and Xcode 9.

In terms of the app structure, there were some independent parts.
For the services, a singleton was created (ServiceManager) to handle the http requests and downloads. Moreover, the functions would already return the data prepared for the View Controllers to simply use.
Since there were only a few view controllers, it was decided to use a storyboard with the necessary character-related view controllers. The Main storyboard was left in order to keep the generic view controller.
The custom views, however, have their own xib file in the same folder as the swift file. They all inherit from the generic view, so that the initial required setup to link the class with the view doesn't need to be replicated.
Some categories were added to add some custom made functions and/or properties, simplifying some processes.
The ThemeManager is the responsible for handling the label themes of the application. In a bigger app, it would also handle the themes of other UI classes, like UIView, UIButton, etc.
The Localizable file was included in order to prepare the app for multilingual support. However, for now only english is available.
Unfortunately, there wasn't time to include Unit and/or UI Testing. Which would have been a good addition, to specially test the services data and the UI Controls robustness.

Improvements

- App Design: it's currently extremely poor
	- Character List View Controller: With a UICollectionView, where each cell would include the thumbnail and name of each hero.
	- Character Details View Controller: Add more data to the sections, instead of just the name
- Unit Testing: nonexistent at the moment
- UI Testing: nonexistent at the moment
- Functionalities:
	* Use the rest of the Marvel services to include more pages, about comics, events, stories and series
	* Core Data: in order to have an offline access

