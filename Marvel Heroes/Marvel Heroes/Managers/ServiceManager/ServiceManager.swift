//
//  ServiceManager.swift
//  Generic Structure
//
//  Created by Afonso Rosa on 07/01/18.
//  Copyright © 2018 Afonso Rosa. All rights reserved.
//

import Foundation
import UIKit

//singleton to handle all the service requests
class ServiceManager {
    
    //baseURL to be used for the services, should be the api base endpoint
    private let baseURL = "https://gateway.marvel.com:443/v1/public/"
    
    private let apiKey = "d487c1ad8e88730a36b02137c5a362ea"
    private let privateApiKey = "3b66fafb18cfe0947a2a78b1d6278045db9c3608"
    
    static let sharedInstance = ServiceManager()
    private init() {} //This prevents others from using the default '()' initializer for this class.
    
    func getAllCharactersWithLimitNumber(_ number:Int, andOffset offset:Int, success: @escaping ((_ data: CharacterList) -> Void), error: @escaping ((_ errorMessage: String) -> Void)) {
        
        let url = "characters?limit=\(number)&offset=\(offset)"
        
        self.getRequestWithUrl(url, success: { (data) in
            
            do {
                let base = try JSONDecoder().decode(CharacterListServiceObject.self, from: data)
                
                success(base.data)
            } catch let jsonError as NSError {
                error(jsonError.localizedDescription)
            }
            
        }) { (requestError) in
            error(requestError)
        }
    }
    
    func getCharactersWithName(_ name: String, limitNumber number: Int, andOffset offset: Int, success: @escaping ((_ data: CharacterList) -> Void), error: @escaping ((_ errorMessage: String) -> Void)) {
        
        let url = "characters?nameStartsWith=\(name)&limit=\(number)&offset=\(offset)"
        
        self.getRequestWithUrl(url, success: { (data) in
            
            do {
                let base = try JSONDecoder().decode(CharacterListServiceObject.self, from: data)
                
                success(base.data)
            } catch let jsonError as NSError {
                error(jsonError.localizedDescription)
            }
            
        }) { (requestError) in
            error(requestError)
        }
    }
    
    func downloadImageFromURLPath(_ urlPath: String, success: @escaping ((_ image: UIImage) -> Void), failure: @escaping ((_ downloadError: Error) -> Void)) {
        guard let url = URL(string: urlPath) else {
            return
        }
        
        self.getDataFromUrl(url: url) { (data, response, error) in
            guard let data = data, error == nil else {
                if error != nil {
                    failure(error!)
                }
                return
            }
            
            if let image = UIImage(data: data) {
                success(image)
            }
        }
    }
    
    //function to process a generic get request
    private func getRequestWithUrl(_ url : String, success: @escaping ((_ data: Data) -> Void), error: @escaping ((_ errorMessage: String) -> Void)) {
        
        var finalURL = self.baseURL + url
        
        let timestamp = String(format: "%.0f", NSDate().timeIntervalSince1970)
        
        let hash = "\(timestamp)\(self.privateApiKey)\(self.apiKey)".utf8.md5
        
        if finalURL.contains("?") {
            finalURL += "&ts=\(timestamp)&apikey=\(self.apiKey)&hash=\(hash)"
        } else {
            finalURL += "?ts=\(timestamp)&apikey=\(self.apiKey)&hash=\(hash)"
        }
        
        if let requestURL = URL(string: finalURL) {
            var request = URLRequest(url: requestURL)
            request.httpMethod = "GET"
            
            let task = URLSession.shared.dataTask(with: request, completionHandler: { (requestData, requestResponse, requestError) in
                
                DispatchQueue.main.async {
                    guard let taskData = requestData, requestError == nil else {                                                 // check for fundamental networking error
                        error("error=\(error)")
                        return
                    }
                    
                    if let httpStatus = requestResponse as? HTTPURLResponse, httpStatus.statusCode != 200 {           // check for http errors
                        print("statusCode should be 200, but is \(httpStatus.statusCode)")
                        error("response = \(String(describing: requestResponse))")
                        return
                    }
                    
                    success(taskData)
                }
            })
            task.resume()
        }
    }

    //function to process a generic data download
    private func getDataFromUrl(url: URL, completion: @escaping (Data?, URLResponse?, Error?) -> ()) {
        URLSession.shared.dataTask(with: url) { data, response, error in
            DispatchQueue.main.async {
                completion(data, response, error)
            }
            }.resume()
    }
}
