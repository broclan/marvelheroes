//
//  FadeAnimator.swift
//  Marvel Heroes
//
//  Created by Afonso Rosa on 13/01/18.
//  Copyright © 2018 Afonso Rosa. All rights reserved.
//

import UIKit

class FadeAnimator: NSObject, UIViewControllerAnimatedTransitioning {
    
    let duration = 1.0
    var presenting = true
    
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return self.presenting ? self.duration : self.duration / 2
    }
    
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        
        let containerView = transitionContext.containerView
        
        guard let toView = transitionContext.view(forKey: .to) else {
            return
        }
        
        containerView.addSubview(toView)
        toView.alpha = 0.0
        
        let duration = self.presenting ? self.duration : self.duration / 2
        
        UIView.animate(withDuration: duration,
                       animations: {
                        toView.alpha = 1.0
        }) { (_) in
            transitionContext.completeTransition(true)
        }
    }
}
