//
//  CharacterDetailsViewController.swift
//  Marvel Heroes
//
//  Created by Afonso Rosa on 13/01/18.
//  Copyright © 2018 Afonso Rosa. All rights reserved.
//

import UIKit

protocol CharacterDetailsViewControllerDelegate : class {
    func didChangeFavourite(_ favourite: Bool)
}

class CharacterDetailsViewController: GSViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    weak var delegate : CharacterDetailsViewControllerDelegate?
    
    var characterName : String?
    var hero : Character?
    var thumbnailImage : UIImage?
    private var favouriteChanged : Bool = false
    private var currentFavouriteStatus : Bool = false {
        didSet {
            self.favouriteChanged = !self.favouriteChanged
        }
    }
    
    fileprivate var sections : [Any] = []
    
    static let kHeaderHeight : CGFloat = 66

    override func viewDidLoad() {
        super.viewDidLoad()

        if let hero = self.hero {
            self.title = hero.name
        } else {
            self.title = self.characterName ?? NSLocalizedString("characterDetails.title", comment: "")
        }
        
        self.addBackButton()
        
        self.tableView.register(UITableViewCell.self, forCellReuseIdentifier: "Cell")
        
        self.tableView.tableFooterView = UIView()
        
        self.getThumbnailImage()
    }
    
    //MARK: - Navigation
    @objc override func goBack() {
        if self.favouriteChanged {
            self.delegate?.didChangeFavourite(self.currentFavouriteStatus)
        }
        super.goBack()
    }
    
    //MARK: - Services
    func getThumbnailImage() {
        self.startLoading()
        
        guard let hero = self.hero else {
            return
        }
        
        let urlString = hero.thumbnail.path + "." + hero.thumbnail.thumbExtension
        
        ServiceManager.sharedInstance.downloadImageFromURLPath(urlString, success: { (image) in
            self.thumbnailImage = image;
            
            self.setupHeaderView()
            
            self.createSections()
            
            self.stopLoading()
        }) { (error) in
            self.setupHeaderView()
            
            self.createSections()
            
            self.stopLoading()
        }
    }
    
    //MARK: - Helpers
    private func createSections() {
        guard let hero = self.hero else {
            return
        }
        
        if hero.comics.items.count > 0 {
            self.sections.append(["Comics", hero.comics.items])
        }
        
        if hero.events.items.count > 0 {
            self.sections.append(["Events", hero.events.items])
        }
        
        if hero.stories.items.count > 0 {
            self.sections.append(["Stories", hero.stories.items])
        }
        
        if hero.series.items.count > 0 {
            self.sections.append(["Series", hero.series.items])
        }
        
        self.tableView.reloadData()
    }
    
    func setupHeaderView() {
        
        guard let hero = self.hero else {
            return
        }
        
        let headerView = CharacterDetailsHeaderView(frame: CGRect(x: 0, y: 0, width: self.tableView.frame.width, height: 0.0))
        
        headerView.delegate = self
        
        headerView.loadWithHeroId(hero.id, name:hero.name, andThumbnail: self.thumbnailImage ?? nil)
        
        headerView.frame = CGRect(x: 0, y: 0, width: headerView.frame.width, height: CharacterDetailsHeaderView.kCharacterDetailsHeaderViewHeight)
        
        self.tableView.tableHeaderView = headerView
    }

}

extension CharacterDetailsViewController : CharacterDetailsHeaderViewDelegate {
    func didChangeFavourite(_ favourite: Bool) {
        self.currentFavouriteStatus = favourite
    }
}

extension CharacterDetailsViewController : UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return self.sections.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section < self.sections.count {
            let sec = self.sections[section] as! [Any]
            
            if let items = sec[1] as? [Any] {
                return items.count < 3 ? items.count : 3
            }
        }
        
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        
        cell.backgroundColor = .white
        cell.textLabel?.setTheme(ThemeManager.darkGrayRegular1)
        
        if indexPath.section < self.sections.count {
            let sec = self.sections[indexPath.section] as! [Any]
            
            if let items = sec[1] as? [Any] {
                if let item = items[indexPath.row] as? ComicItem {
                    cell.textLabel?.text = item.name
                } else if let item = items[indexPath.row] as? EventItem {
                    cell.textLabel?.text = item.name
                } else if let item = items[indexPath.row] as? StoryItem {
                    cell.textLabel?.text = item.name
                } else if let item = items[indexPath.row] as? SeriesItem {
                    cell.textLabel?.text = item.name
                }
            }
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        if section < self.sections.count {
            let sec = self.sections[section] as! [Any]
            
            if let items = sec[1] as? [Any], items.count > 0 {
                return CharacterDetailsViewController.kHeaderHeight
            }
        }
        
        return 0
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let headerView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: self.tableView.frame.width, height: CharacterDetailsViewController.kHeaderHeight))
        
        let headerLabel = UILabel.init(frame: CGRect.init(x: 20, y: 10, width: headerView.frame.width - 40, height: headerView.frame.height - 20))
        
        headerLabel.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        headerLabel.setTheme(ThemeManager.blackRegular1)
        
        if section < self.sections.count {
            let sec = self.sections[section] as! [Any]
            
            if let header = sec[0] as? String {
                headerLabel.text = header
            }
        }
        
        headerView.addSubview(headerLabel)
        
        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.00001
    }

}
