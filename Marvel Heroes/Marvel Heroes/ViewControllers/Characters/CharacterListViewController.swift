//
//  CharacterListViewController.swift
//  Marvel Heroes
//
//  Created by Afonso Rosa on 13/01/18.
//  Copyright © 2018 Afonso Rosa. All rights reserved.
//

import UIKit

class CharacterListViewController: GSViewController {
    
    @IBOutlet weak var searchView: SearchView!
    @IBOutlet weak var tableView: UITableView!
    
    private let pageSize = 20
    private var offset = 0
    fileprivate var characters : [Character]?
    fileprivate var currentName : String = ""
    fileprivate var canLoadMore = true
    fileprivate var isLoading = false

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = NSLocalizedString("characterList.title", comment: "")
        
        self.searchView.delegate = self
        
        self.tableView.register(UINib(nibName: "CharacterListTableViewCell", bundle: nil), forCellReuseIdentifier: CharacterListTableViewCell.identifier)
        
        self.tableView.tableFooterView = UIView()

        self.getCharacters()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    //MARK: - Services
    func getCharacters() {
        
        if self.isLoading || !self.canLoadMore  {
            return
        } else {
            self.isLoading = true
        }
        
        self.startLoading()
        
        ServiceManager.sharedInstance.getAllCharactersWithLimitNumber(self.pageSize, andOffset: self.offset, success: { (characterList) in
            
            if characterList.total == 0 {
                self.canLoadMore = false
            }
            
            if let characters = self.characters, self.offset > 0 {
                self.characters?.append(contentsOf: characterList.results)
                
                if characterList.total == Float(characters.count) {
                    self.canLoadMore = false
                }
                
            } else {
                self.characters = characterList.results
                
                if characterList.total == Float(characterList.results.count) {
                    self.canLoadMore = false
                }
            }
            
            self.tableView.reloadData()
            self.offset += self.pageSize
            self.isLoading = false
            self.stopLoading()
        }) { (error) in
            print(error)
            self.isLoading = false
            self.stopLoading()
        }
    }
    
    func getCharactersWithName(_ name: String) {
        if self.isLoading || !self.canLoadMore {
            return
        } else {
            self.isLoading = true
        }
        
        self.startLoading()
        
        ServiceManager.sharedInstance.getCharactersWithName(name, limitNumber: self.pageSize, andOffset: self.offset, success: { (characterList) in
            
            if characterList.total == 0 {
                self.canLoadMore = false
            }
            
            if let characters = self.characters, self.offset > 0 {
                self.characters?.append(contentsOf: characterList.results)
                
                if characterList.total == Float(characters.count) {
                    self.canLoadMore = false
                }
                
            } else {
                self.characters = characterList.results
                
                if characterList.total == Float(characterList.results.count) {
                    self.canLoadMore = false
                }
            }
            
            self.tableView.reloadData()
            self.offset += self.pageSize
            self.isLoading = false
            self.stopLoading()
        }) { (error) in
            print(error)
            self.isLoading = false
            self.stopLoading()
        }
    }
}

extension CharacterListViewController : SearchViewDelegate {
    func searchViewSearchButtonClicked(_ searchView: SearchView, withText text: String) {
        
        if self.currentName == text {
            return;
        }
        
        self.offset = 0
        self.currentName = text
        self.canLoadMore = true
        if text == "" {
            self.getCharacters()
        } else {
            self.getCharactersWithName(text)
        }
    }
}

extension CharacterListViewController : UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let characters = self.characters else {
            return 0
        }
        return characters.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return CharacterListTableViewCell.preferrableHeight
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let characters = self.characters else {
            return UITableViewCell()
        }
        
        if let cell = tableView.dequeueReusableCell(withIdentifier: CharacterListTableViewCell.identifier, for: indexPath) as? CharacterListTableViewCell {
            
            let character = characters[indexPath.row]
            
            cell.loadWithHeroName(character.name, andId: character.id)
            
            if self.canLoadMore && indexPath.row == characters.count - 2, self.currentName == "" {
                if self.currentName == "" {
                    self.getCharacters()
                } else {
                    self.getCharactersWithName(self.currentName)
                }
            }
            
            return cell
        }
        
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        guard let characters = self.characters else {
            return
        }
        
        let storyboard = UIStoryboard(name: "Characters", bundle: nil)
        let vc : CharacterDetailsViewController = storyboard.instantiateViewController(withIdentifier: "CharacterDetailsViewController") as! CharacterDetailsViewController
        
        vc.hero = characters[indexPath.row]
        vc.delegate = self
        
        self.goToViewController(vc)
    }
}

extension CharacterListViewController : CharacterDetailsViewControllerDelegate {
    func didChangeFavourite(_ favourite: Bool) {
        self.tableView.reloadData()
    }
}
