//
//  CharacterList.swift
//  Marvel Heroes
//
//  Created by Afonso Rosa on 13/01/18.
//  Copyright © 2018 Afonso Rosa. All rights reserved.
//

import UIKit

struct CharacterList: MarvelDataObject {
    
    var offset: Float
    
    var limit: Float
    
    var total: Float
    
    var count: Float

    var results : [Character]
    
}
