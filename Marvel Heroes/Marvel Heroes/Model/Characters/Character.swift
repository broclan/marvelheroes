//
//  Character.swift
//  Marvel Heroes
//
//  Created by Afonso Rosa on 13/01/18.
//  Copyright © 2018 Afonso Rosa. All rights reserved.
//

import Foundation

struct Character : Codable {
    
    var id : Float
    var name : String
    var description : String
    var modified : String
    var resourceURI : String
    var urls : [URLObject]
    var thumbnail : ThumbnailObject
    var comics : CommonComicObject
    var stories : CommonStoryObject
    var events : CommonEventObject
    var series : CommonSeriesObject
}
