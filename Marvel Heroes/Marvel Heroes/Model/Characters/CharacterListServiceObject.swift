//
//  CharacterListServiceObject.swift
//  Marvel Heroes
//
//  Created by Afonso Rosa on 13/01/18.
//  Copyright © 2018 Afonso Rosa. All rights reserved.
//

import Foundation

struct CharacterListServiceObject: BaseServiceObject {
    
    var code: Float
    
    var status: String
    
    var copyright: String
    
    var attributionText: String
    
    var attributionHTML: String
    
    var etag: String
    
    var data: CharacterList
    
}
