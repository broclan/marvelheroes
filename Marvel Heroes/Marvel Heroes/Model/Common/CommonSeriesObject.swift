//
//  CommonSeriesObject.swift
//  Marvel Heroes
//
//  Created by Afonso Rosa on 13/01/18.
//  Copyright © 2018 Afonso Rosa. All rights reserved.
//

import Foundation

struct CommonSeriesObject : Codable {
    
    var available : Float
    var returned : Float
    var collectionURI : String
    var items : [SeriesItem]
}
