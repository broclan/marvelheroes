//
//  EventItem.swift
//  Marvel Heroes
//
//  Created by Afonso Rosa on 13/01/18.
//  Copyright © 2018 Afonso Rosa. All rights reserved.
//

import Foundation

struct EventItem : ItemObject {
    
    var resourceURI: String
    
    var name: String
}
