//
//  ComicItem.swift
//  Marvel Heroes
//
//  Created by Afonso Rosa on 13/01/18.
//  Copyright © 2018 Afonso Rosa. All rights reserved.
//

import Foundation

struct ComicItem : ItemObject {
    
    var resourceURI: String
    
    var name: String
}
