//
//  StoryITem.swift
//  Marvel Heroes
//
//  Created by Afonso Rosa on 13/01/18.
//  Copyright © 2018 Afonso Rosa. All rights reserved.
//

import Foundation

struct StoryItem : ItemObject {
    
    var resourceURI: String
    
    var name: String
    
    var type : String
}
