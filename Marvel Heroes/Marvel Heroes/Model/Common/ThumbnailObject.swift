//
//  ThumbnailObject.swift
//  Marvel Heroes
//
//  Created by Afonso Rosa on 13/01/18.
//  Copyright © 2018 Afonso Rosa. All rights reserved.
//

import Foundation

struct ThumbnailObject : Codable {
    
    var path : String
    var thumbExtension : String
    
    enum CodingKeys: String, CodingKey {
        case path = "path"
        case thumbExtension = "extension"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        
        self.path = try values.decode(String.self, forKey: .path)
        self.thumbExtension = try values.decode(String.self, forKey: .thumbExtension)
    }
}
