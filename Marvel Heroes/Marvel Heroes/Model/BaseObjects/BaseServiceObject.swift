//
//  BaseServiceObject.swift
//  Marvel Heroes
//
//  Created by Afonso Rosa on 12/01/18.
//  Copyright © 2018 Afonso Rosa. All rights reserved.
//

import Foundation

protocol BaseServiceObject: Codable {
    
    var code: Float { get set }
    var status: String { get set }
    var copyright: String { get set }
    var attributionText: String { get set }
    var attributionHTML: String { get set }
    var etag: String { get set }

}
