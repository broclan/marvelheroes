//
//  ItemObject.swift
//  Marvel Heroes
//
//  Created by Afonso Rosa on 13/01/18.
//  Copyright © 2018 Afonso Rosa. All rights reserved.
//

import Foundation

protocol ItemObject : Codable {
    
    var resourceURI : String { get set }
    var name : String { get set }
}
