//
//  MarvelDataObject.swift
//  Marvel Heroes
//
//  Created by Afonso Rosa on 12/01/18.
//  Copyright © 2018 Afonso Rosa. All rights reserved.
//

import Foundation

protocol MarvelDataObject: Codable {
    
    var offset: Float { get set }
    var limit: Float { get set }
    var total: Float { get set }
    var count: Float { get set }
    
}
