//
//  CharacterDetailsHeaderView.swift
//  Marvel Heroes
//
//  Created by Afonso Rosa on 15/01/18.
//  Copyright © 2018 Afonso Rosa. All rights reserved.
//

import UIKit

protocol CharacterDetailsHeaderViewDelegate : class {
    func didChangeFavourite(_ favourite: Bool)
}

class CharacterDetailsHeaderView: GSView {
    
    static let kCharacterDetailsHeaderViewHeight : CGFloat = 120
    
    @IBOutlet weak var heroImageView: UIImageView!
    @IBOutlet weak var heroImageViewWidth: NSLayoutConstraint!
    @IBOutlet weak var heroLabel: UILabel!
    @IBOutlet weak var heroLabelLeftConstraint: NSLayoutConstraint!
    @IBOutlet weak var favouriteImageView: UIImageView!
    
    weak var delegate : CharacterDetailsHeaderViewDelegate?
    
    fileprivate var heroId: Float?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    private func setup() {
        self.view.backgroundColor = .mRed1
        
        let tap = UITapGestureRecognizer.init(target: self, action: #selector(didTouchFavouriteImageView))
        self.favouriteImageView.addGestureRecognizer(tap)
    }
    
    func loadWithHeroId(_ id:Float, name: String, andThumbnail thumbnail:UIImage?) {
        
        self.heroId = id
        
        self.heroLabel.text = String.init(format: "%@ %@", name, NSLocalizedString("characterDetails.details", comment: ""))
        
        if let image = thumbnail {
            self.heroImageView.image = image
            self.heroImageViewWidth.constant = 100
            self.heroLabelLeftConstraint.constant = 5
        } else {
            self.heroImageViewWidth.constant = 0
            self.heroLabelLeftConstraint.constant = 0
        }
        
        let favouriteKey = "Hero_" + String(format: "%.0f", id)
        
        let isFavourite = UserDefaults.standard.bool(forKey: favouriteKey)
        
        self.processFavouriteImageView(isFavourite)
    }
    
    //MARK: - Actions
    @objc private func didTouchFavouriteImageView() {
        
        guard let id = self.heroId else {
            return
        }
        
        let favouriteKey = "Hero_" + String(format: "%.0f", id)
        
        let isFavourite = UserDefaults.standard.bool(forKey: favouriteKey)
        
        UserDefaults.standard.set(!isFavourite, forKey: favouriteKey)
        
        self.processFavouriteImageView(!isFavourite)
        
        self.delegate?.didChangeFavourite(!isFavourite)
    }
    
    //MARK: - Helpers
    private func processFavouriteImageView(_ isFavourite : Bool) {
        if isFavourite {
            self.favouriteImageView.image = #imageLiteral(resourceName: "star")
        } else {
            self.favouriteImageView.image = #imageLiteral(resourceName: "star_unselected")
        }
    }

}
