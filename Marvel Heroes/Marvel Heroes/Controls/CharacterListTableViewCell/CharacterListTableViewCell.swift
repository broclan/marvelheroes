//
//  CharacterListTableViewCell.swift
//  Marvel Heroes
//
//  Created by Afonso Rosa on 14/01/18.
//  Copyright © 2018 Afonso Rosa. All rights reserved.
//

import UIKit

class CharacterListTableViewCell: UITableViewCell {
    
    static let identifier = "CharacterListTableViewCell"
    static let preferrableHeight : CGFloat = 44

    @IBOutlet weak var favouriteImageView: UIImageView!
    @IBOutlet weak var heroNameLabel: UILabel!
    
    fileprivate var heroId: Float?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.setup()
    }
    
    private func setup() {
        self.accessoryType = .disclosureIndicator
        self.backgroundColor = .white
        
        let tap = UITapGestureRecognizer.init(target: self, action: #selector(didTouchFavouriteImageView))
        self.favouriteImageView.addGestureRecognizer(tap)
    }
    
    func loadWithHeroName(_ name: String, andId id:Float) {
        self.heroNameLabel.text = name
        
        self.heroId = id
        
        let favouriteKey = "Hero_" + String(format: "%.0f", id)
        
        let isFavourite = UserDefaults.standard.bool(forKey: favouriteKey)
        
        self.processFavouriteImageView(isFavourite)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
    //MARK: - Actions
    @objc private func didTouchFavouriteImageView() {
        
        guard let id = self.heroId else {
            return
        }
        
        let favouriteKey = "Hero_" + String(format: "%.0f", id)
        
        let isFavourite = UserDefaults.standard.bool(forKey: favouriteKey)
        
        UserDefaults.standard.set(!isFavourite, forKey: favouriteKey)
        
        self.processFavouriteImageView(!isFavourite)
    }
    
    //MARK: - Helpers
    private func processFavouriteImageView(_ isFavourite : Bool) {
        if isFavourite {
            self.favouriteImageView.image = #imageLiteral(resourceName: "star")
        } else {
            self.favouriteImageView.image = #imageLiteral(resourceName: "star_unselected")
        }
    }
}
