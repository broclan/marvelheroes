//
//  SearchView.swift
//  Marvel Heroes
//
//  Created by Afonso Rosa on 14/01/18.
//  Copyright © 2018 Afonso Rosa. All rights reserved.
//

import UIKit

@objc protocol SearchViewDelegate {
    
    @objc optional func searchViewSearchButtonClicked(_ searchView: SearchView, withText text: String)
}

class SearchView: GSView {

    @IBOutlet weak var searchBar: UISearchBar!
    
    weak var delegate: SearchViewDelegate?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    private func setup() {
        self.searchBar.barTintColor = .mRed1
    }

}

extension SearchView : UISearchBarDelegate {
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        if let text = searchBar.text {
            self.delegate?.searchViewSearchButtonClicked?(self, withText: text)
        }
        searchBar.resignFirstResponder()
    }
}
